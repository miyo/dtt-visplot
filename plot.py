
from gwpy.plot import BodePlot as GwBodePlot

class BodePlot(GwBodePlot):
    def __init__(self,*filters,**kwargs):
        label = kwargs.pop('label', True)
        super().__init__(*filters,**kwargs)
        ylims = [[-120,20],[-180,180]]
        [ax.set_ylim(*ylim) for ax,ylim in zip(self.axes,ylims)]
        self.axes[1].set_yticks(range(-180,181,90))
        [ax.legend(label,alpha=0.7,loc='upper left') for ax in self.axes]
        
    def add_coherence(self):
        self.clf()
        self.add_subplot(3,1,1)
        self.add_subplot(3,1,2,sharex=self.axes[2])
        self.add_subplot(3,1,3,sharex=self.axes[2])
        
